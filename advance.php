<!DOCTYPE html>
<html lang="en">
<head>

	<title>My Advance Chat</title>

	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="asset/bootstrap4.5.3/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

	<script src="https://kit.fontawesome.com/18a5332e58.js" crossorigin="anonymous"></script>

	<!--load all styles -->
	<link href="asset/fontawesome5/css/all.css" rel="stylesheet"> 
	<script defer src="asset/fontawesome5/js/all.js"></script> <!--load all styles -->

	<script src="asset/jquery-3.5.1.min.js"></script>
	<!-- Custom Script -->
	<script type="text/javascript">
		
		function update() {
			
			$.post("_incl/con_multiple.php", {}, function(data){ $("#screen").html(data);});  
			setTimeout('update()', 1000);
		}

		function lastChat() {
			// Scroll to last  message
			var $chat = $(".chat-area");
			$chat.scrollTop($chat.height());
		}

		$(document).ready(function(){
			update();
			lastChat();
			
			$("#button").click(function() {         
				$.post("_incl/con_multiple.php",{ message: $("#message").val(), user: $("#userAccount").val()},
				function(data) { 
					$("#screen").html(data); 
					$("#message").val("");
				});
			});
		});

	</script>


	<style type="text/css">

		div.chat-box {
			background-color: #9ac4d6;
			border: 2px solid #116c75;
		    border-radius: 5px;
		    padding: 5%;
    	}

		/* 
		##Device = Laptops, Desktops
		##Screen = B/w 1025px to 1280px
		*/
		@media (min-width: 1024px) and (max-width: 2879px) {
			body {
				padding: 5% 10% !important;
			}

			div.chat-window {
				padding: 1% 15% 1%;
			}
		}

		/* 
		##Screen = B/w 350px to 1024px
		*/
		@media (min-width: 350px) and (max-width: 1024px) {
			body {
				padding: 10% 1% !important;
			}

			div.chat-window {
				padding: 1% 10% 1%;
			}
		}

		@media (min-width: 5px) and (max-width: 350px) {
			body {
				padding: 10% 1% !important;
			}

			div.chat-window {
				padding: 1% 5% 1%;
			}

			div.chat-box {
			    padding: 2%;
	    	}
		}


    	label {
    		font-weight: 500;
    	}

    	div.chat-area {
    		background-color: #fff;
    		border-radius: 5px;
    		overflow-y: scroll;
    		min-height: 80%;
    		height: 300px;
    		padding: 2%;
    	}

    	.left-chat p, .right-chat p {
    		padding: 1% 3%;
    		font-size: 14px;
    		/*text-align: justify;*/
    	}

    	.left-chat div, .right-chat div{
			border-top-left-radius: 0px;
		    border-top-right-radius: 0px;
		    border-bottom-right-radius: 10px;
		    border-bottom-left-radius: 10px;
    	}
    	.left-chat div {
		    background-color: #5dd076;
		    margin-right: 30%;
    	}
    	.left-chat p{
    	}

    	.right-chat div{
		    background-color: #4fb6e6;
		    margin-left: 30%;
    	}
    	.right-chat p{
    		color: #fff;
    	}

    	.sms-chat > div  hr {
    		border-top: 1px solid rgb(255 255 255);
    		margin: 0px;
    	}
    	.sms-chat > div > label.sender-info {
		    font-family: monospace;
    		padding: 0px 3%;
			font-size: 11px;
			color: #6f1a1a;
    		width: 100%;
    	}

    	.left {
    		float: left;
    	}
    	.allgin-end {
    		text-align: end;
    	}
	</style>
</head>
<body>

	<div class="container">
	  <!-- Content here -->
		<div class="chat-window">
			<div class="chat-box">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="chat-area" id="screen">

							</div>
							<!-- <textarea class="form-control" id="screen" rows="18" resize="no"></textarea> -->
						</div>
					</div>
				</div> 

				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<label for="userAccount">Sender Name</label>
							<select class="form-control" id="userAccount">
								<option value="john">John</option>
								<option value="nick">Nick</option>
							</select>
						</div>
					</div>

					<div class="col-md-8 col-sm-12"> 
						<div class="form-group">
							<label for="userMessage">Message</label>
							<input type="text" class="form-control" id="message" placeholder="say something...">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<button type="button" class="btn btn-primary btn-lg btn-block" id="button">
							<i class="fas fa-comments"></i> Send Message
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="asset/bootstrap4.5.3/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>


</body>
</html>
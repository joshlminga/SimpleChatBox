<!DOCTYPE html>
<html>
<head>
	<title>My Simple Chat</title>

	<!-- Script -->
	<script type="text/javascript" src="asset/jquery-3.5.1.min.js"></script>

	<!-- <script type="text/javascript" src="asset/jquery-1.2.6.pack.js"></script> -->

	<script type="text/javascript">
		
		function update() {
			$.post("_incl/con_single.php", {}, function(data){ $("#screen").val(data);});  
			setTimeout('update()', 1000);
		}

		$(document).ready(function(){
			update();

			$("#button").click(function() {         
				$.post("_incl/con_single.php",{ message: $("#message").val()},
				function(data) { 
					$("#screen").val(data); 
					$("#message").val("");
				});
			});
		});

	</script>
</head>
<body>

	<!-- Message View -->
	<textarea id="screen" cols="40" rows="40"> </textarea> <br>  

	<!-- Write Box -->
	<input id="message" size="40"> <br />

	<!-- Send Button -->
	<button id="button"> Send </button>

</body>
</html>
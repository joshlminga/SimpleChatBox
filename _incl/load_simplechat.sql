-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 16, 2020 at 10:35 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `load_simplechat`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `Id` int(11) NOT NULL,
  `box` int(11) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `message` text NOT NULL,
  `chat` varchar(10) DEFAULT 'single',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`Id`, `box`, `user`, `message`, `chat`, `stamp`) VALUES
(61, NULL, NULL, 'Test Again', 'multiple', '2020-12-16 20:51:16'),
(60, NULL, NULL, 'Kimanga', 'single', '2020-12-16 20:51:16'),
(59, NULL, NULL, 'All who sent', 'single', '2020-12-16 20:51:16'),
(58, NULL, NULL, 'Nzaga', 'single', '2020-12-16 20:51:16'),
(57, NULL, NULL, 'Amos 2', 'single', '2020-12-16 20:51:16'),
(56, NULL, NULL, 'John', 'single', '2020-12-16 20:51:16'),
(62, NULL, NULL, 'more', 'single', '2020-12-16 20:51:16'),
(63, NULL, NULL, 'Timo', 'single', '2020-12-16 20:51:16'),
(64, NULL, NULL, 'DT', 'single', '2020-12-16 20:51:16'),
(65, NULL, 'john', 'sdsa', 'multiple', '2020-12-16 20:51:16'),
(66, NULL, 'nick', 'Hello', 'multiple', '2020-12-16 20:51:16'),
(67, NULL, 'john', 'Black', 'multiple', '2020-12-16 20:51:16'),
(68, NULL, 'nick', 'Hello once again', 'multiple', '2020-12-16 20:51:16'),
(69, NULL, 'nick', 'Success chat', 'multiple', '2020-12-16 22:32:29'),
(70, NULL, 'john', 'we are here', 'multiple', '2020-12-16 22:32:56'),
(71, NULL, 'nick', 'Done', 'multiple', '2020-12-16 22:33:06'),
(72, NULL, 'john', 'configure', 'multiple', '2020-12-16 22:34:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

	// Database Connection
	include 'database.php';

	/*
	* Post Message
	*/
	$message = $_POST['message'];

	// Create connection
	$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	/*
	* Save Message
	*
	* Save message if request is not empty
	*/
	if($message != ""){
		$sql = "INSERT INTO chat (message)
		VALUES ('$message')";

		if ($conn->query($sql) === TRUE) {
		  //echo "New record created successfully";
		} else {
		  echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}

	/*
	* Select Message
	*
	* Show each message (Echo Message)
	*/
	$sql = "SELECT message FROM chat WHERE chat= 'single' ORDER BY `Id` DESC";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			echo $row["message"]. "\n";
		}
	}else {
		echo "Start Typing...";
	}
		
	$conn->close();
?>
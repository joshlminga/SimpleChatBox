<?php

	// Database Connection
	include 'database.php';

	/*
	* Post Message
	*/
	$message = $_POST['message'];
	$user = strtolower(trim($_POST['user']));

	// Create connection
	$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	/*
	* Save Message
	*
	* Save message if request is not empty
	*/
	if($message != ""){
		$sql = "INSERT INTO chat (user,message,chat)
		VALUES ('$user','$message','multiple')";

		if ($conn->query($sql) === TRUE) {
		  //echo "New record created successfully";
		} else {
		  echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}

	/*
	* Select Message
	*
	* Show each message (Echo Message)
	*/
	$sql = "SELECT user,message,stamp FROM chat WHERE chat= 'multiple' ORDER BY `Id` ASC";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {

			$prevUser = ucwords($row["user"]);
			$prevChat = $row["message"];
			$prevStamp = date_format(date_create($row["stamp"]),"M d, Y H:i");

			if (strtolower($prevUser) == 'john') {
				echo "

					<div class='left-chat sms-chat'>
						<div>
							<label class='sender-info'>$prevUser | $prevStamp <hr></label>
							<p>$prevChat</p>
						</div>
					</div>

				";
			}else{
				echo "

					<div class='right-chat sms-chat'>
						<div>
							<label class='sender-info allgin-end'>$prevUser | $prevStamp <hr></label>
							<p>$prevChat</p>
						</div>
					</div>

				";
			}			

			//echo $row["message"]. "\n";
		}
	}else {
		echo "Start Typing...";
	}
		
	$conn->close();
?>